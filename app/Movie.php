<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    public $table = "movie";
    protected $primaryKey = 'mov_id';

    public static function getMovieDetails($mov_id){
        return self::find($mov_id);
    }

    public function directors(){
        return $this->belongsToMany('App\Director', 'movie_direction', 'mov_id', 'dir_id');
    }

    public function genres(){
        return $this->belongsToMany('App\Genre', 'movie_genres', 'mov_id', 'gen_id');
    }

    public function actors(){
        return $this->belongsToMany('App\Actor', 'movie_cast', 'mov_id', 'act_id')->withPivot('role');
    }

    public function reviewers(){
        return $this->belongsToMany('App\Reviewer', 'rating', 'mov_id', 'rev_id')->withPivot('rev_stars');
    }

    public static function getAllMovies(){
        return self::paginate(10);
    }
}
