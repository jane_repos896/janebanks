<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public function rating(){
        return $this->hasMany('App\Reviewer','rev_id','rev_id');
    }
}
