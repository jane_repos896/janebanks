<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieDirection extends Model
{
    public function showAllDirectors(){
        return $this->belongsToMany('App\Movie', 'movie_genres', 'gen_id', 'mov_id');
    }
}
