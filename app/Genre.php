<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $primaryKey = 'gen_id';

    public function genres(){
        return $this->belongsToMany('App\Movie', 'movie_genres', 'gen_id', 'mov_id');
    }
}
