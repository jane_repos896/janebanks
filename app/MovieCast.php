<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieCast extends Model
{
    public function showAllActors(){
        return $this->hasMany('App\Actors','act_id','act_id');
    }

}
