<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieGenre extends Model
{
    public function movie_genres(){
        return $this->hasMany('App\Genre','gen_id','gen_id');
    }
}
