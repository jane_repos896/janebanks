<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Director;
use App\MovieDirection;

class MovieController extends Controller
{
    public function list(){
        $allMovies = Movie::getAllMovies();

        return view('movies/movieList',compact('allMovies'));
    }

    public function viewDetails(Request $request){
        $movie = Movie::getMovieDetails($request->mov_id);

        return view('movies/viewDetails',compact('movie'));
    }

}
