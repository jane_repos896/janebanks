<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviewer extends Model
{
    public $table = "reviewer";
    protected $primaryKey = 'rev_id';

    public function reviewers(){
        return $this->belongsToMany('App\Movie', 'rating', 'rev_id', 'mov_id');
    }
    
}
