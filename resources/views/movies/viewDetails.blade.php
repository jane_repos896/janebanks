@extends('movies/pagelayout')
@section('content')
<section class="row" style="margin-top: 50px; height: 73vh;">
    <section class="col-sm-1"></section>
    <section class="col-sm-4">
    <h1>Movie Information</h1>
        <table class="table" id="messagestable">
            <tr><td>Movie Title: </td><td>{{$movie->mov_title}}</td></tr>
            <tr><td>Year: </td><td>{{$movie->mov_year}}</td></tr>
            <tr><td>Running Time: </td><td>{{$movie->mov_time}} minutes</td></tr>
            <tr><td>Directed By: </td>
                <td>
                    @foreach ($movie->directors as $director)
                        {{ $director->dir_fname }} {{ $director->dir_lname }}
                    @endforeach
                </td>
            </tr>
            <tr><td>Starring: </td>
                <td>
                    @foreach ($movie->actors as $actor)
                        {{ $actor->act_fname }} {{ $actor->act_lname }}
                    @endforeach 
                     - 
                    @foreach ($movie->actors as $role)
                        {{ $role->pivot->role }}
                    @endforeach 
                </td>
            </tr>
            <tr><td>Genre: </td>
                <td>
                    @foreach ($movie->genres as $genre)
                        {{ $genre->gen_title }}
                    @endforeach
                </td>
            </tr>
            <tr><td>Rating: </td>
                <td>
                    @foreach ($movie->reviewers as $reviewer)
                        {{ $reviewer->rev_name }}
                    @endforeach
                </td>
            </tr>
            <tr><td>Score: </td>
                <td>
                    @foreach ($movie->reviewers as $star)
                        {{ $star->pivot->rev_stars }}
                    @endforeach stars
                </td>
            </tr>
        </table>
    
    </section>
    <section class="col-sm-1"></section>
</section>
<section class="row" style="margin:0;">
    <section class="col-sm-5" style="margin-left:100px; justify-content:center;">
    <a href="{{ url('movies/movieList') }}" class="btn">Go Back to List</a>
    </section>
</section>
@endsection
