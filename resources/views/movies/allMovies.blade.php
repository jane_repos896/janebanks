<section class="row" style="margin-top: 20px; height: 85vh;">
    <section class="col-sm-1"></section>
    <section class="col-sm-10">
        <table class="table" id="messagestable">
            <thead>
                <tr class="table-head">
                    <th>Movie ID</th>
                    <th>Movie Title</th>
                    <th>Year Made</th>
                    <th>Length</th>
                    <th>Language</th>
                    <th>Date of Release</th>
                    <th>Country Released</th>
                    <th>View Details</th>
                </tr>
            </thead>
            <tbody>
            @foreach($allMovies as $movie)
                <tr>
                    <td>{{ $movie->mov_id }}</td>
                    <td>{{ $movie->mov_title }}</td>
                    <td>{{ $movie->mov_year }}</td>
                    <td>{{ $movie->mov_time }}</td>
                    <td>{{ $movie->mov_lang }}</td>
                    <td>{{ $movie->mov_dt_rel }}</td>
                    <td>{{ $movie->mov_rel_country }}</td> 
                    
                    <td style="width:9em;">
                        <a href="{{ url('movies/viewDetails/'.$movie->mov_id) }}" class="btn">Movie Details</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        
    </section>
    <section class="col-sm-1"></section>
</section>
<section class="row" style="margin:0;">
    <section class="col-sm-12" style="display:flex; justify-content:center;">
        {{ $allMovies->links() }}
    </section>
</section>

